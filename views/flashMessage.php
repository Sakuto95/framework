<br><br>
<div class="alert alert-dismissible alert-info">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <?= $message; ?>
</div>