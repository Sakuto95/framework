<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" media="screen" href="/assets/style.css"/>
    <meta name="description" content="<?= isset($description_for_layout) ? $description_for_layout : 'Test Project'; ?>">
    <title><?= isset($title_for_layout) ? $title_for_layout : 'Test Project'; ?></title>

    <base href="<?= WEBSITE; ?>/">
</head>
<body>
    <?= $content_for_layout; ?>
</body>
</html>