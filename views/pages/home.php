<?php
$lastNews = Dispatcher::requestAction('News', 'getLastNews');
?>
<h2>Les comptes snapchat de Star</h2>
<p>Snapchat est une application qui permet d'envoyer des photos ou videos à vos amis avec votre mobile. Vos amis ont alors quelques secondes pour regarder votre photo ou video sinon celle ci disparait, tout cela fait le charme de l'application qui cartonne</p>
<p>L'application est de plus en plus utilisé par les stars. Sur Snapchatdestar.fr nous vous donnons en exclusivité les Snapchat de stars de télé réalité, animateurs, acteurs, youtubeurs...</p>

<div class="separator"></div>

<div id="news">
    <h3>Les news</h3>

    <?php foreach($lastNews as $n): ?>
        <div class="news">
            <img src="assets/img/news/<?= $n->id; ?>.png" alt="<?= $n->title; ?>">
            <h4><a href="/news/show/<?= $n->id; ?>"><?= $n->title; ?></a></h4>
            <p><?= substr(nl2br($n->content), 0, 300); ?> ... <a href="/news/show/<?= $n->id; ?>">Lire la suite</a></p>

            <div class="clear"></div>
        </div>
    <?php endforeach; ?>

    <div class="pagination">
        <a href="/news/listing">Afficher toutes les news</a>
    </div>
</div>