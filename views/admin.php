<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Snapchat de stars</title>
    <base href="<?= WEBSITE; ?>/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="stylesheet" href="/assets/bootstrap3/css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="/assets/bootstrap3/css/bootswatch.css" media="screen">
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="/admin" class="navbar-brand">Snapchats de Stars</a>
        </div>

        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
                <li><a href="/categories/admin_listing">Catégories</a></li>
                <li><a href="/news/admin_listing">News</a></li>
                <li><a href="/snapchat/admin_listing">Snapchats</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if($_SESSION['User']->type == 1): ?>
                    <li><a href="/Admin/logout">Déconnexion</a></li>
                <?php endif; ?>
                <li><a href="/">Retour au site</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 60px;">
    <?= $Session->getFlash(); ?>
    <?= $content_for_layout; ?>
</div>
<script src="/assets/js/jquery-2.1.1.min.js"></script>
<script src="/assets/js/admin.js"></script>
</body>
</html>