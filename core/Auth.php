<?php
class Auth {
    private static $Session;
    private static $bdd;
    private static $dispatcher;
    private static $denied =
        [
            // Visiteur
            0 => [
                'categories/admin_edit',
                'categories/admin_listing',
                'categories/admin_delete',
                'news/admin_edit',
                'news/admin_listing',
                'news/admin_delete',
                'snapchat/admin_edit',
                'snapchat/admin_listing',
                'snapchat/admin_delete'
            ],

            1 => []
        ];

    /**
     * Instanciate an instance of the database and dispatcher
     * @param $dispatcher
     */
    public function __construct($dispatcher) {
        self::$bdd = Database::getInstance();
        self::$dispatcher = $dispatcher;

        self::$Session = Session::getInstance();
    }

    /**
     * Try to login a user, return the number of information found
     * @param $user
     * @param $password
     * @return int
     */
    public static function login($user, $password) {
        $req = self::$bdd->prepare("SELECT * FROM users WHERE username=? AND password=?");
        $req->execute([$user, sha1($password)]);

        if($req->rowCount() == 0) return false;

        $req->setFetchMode(PDO::FETCH_OBJ);
        $data = $req->fetch();
        return $data->id;
    }

    /**
     * Test if user is loggued
     * @return bool
     */
    public static function isLoggued() {
        return isset($_SESSION['User']);
    }

    /**
     * Check if the user can be in the action
     * @return int
     */
    public static function isAuthorized() {
        $userType = isset(self::$Session->User) ? self::$Session->User->type : 0;
        $action = self::$dispatcher->getController()."/".self::$dispatcher->getAction();

        if(in_array($action, self::$denied[$userType]))
            return 0;

        return 1;
    }
}