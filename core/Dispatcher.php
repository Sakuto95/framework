<?php

class Dispatcher
{
    private $controller;
    public $Session;
    private $action;
    private $args = [];

    public $routes = [];
    public $url;

    // Ajout pour g�rer le router
    public function add($path, $action)
    {
        $route = new Route($path, $action);
        $this->routes[] = $route;

        return $route;
    }

    public function run()
    {
        foreach ($this->routes as $route) {
            if ($route->match($this->url)) {
                $params = explode('#', $route->callable);

                $this->controller = strtolower($params[0]);
                $this->action = strtolower($params[1]);
                $this->args = $route->matches;

                return true;
            }
        }

        $args = explode('/', $this->url);
        array_shift($args);

        $this->controller = array_shift($args);
        $this->action = array_shift($args);
        $this->args = $args;

        if (empty($this->action)) {
            if (empty($this->controller)) {
                $this->controller = 'pages';
            }
            $this->action = 'home';
        }

        $this->dispatch();
    }
    // Fin des ajouts

    /**
     * Construct the element
     * Retrieve the controller and the action depending of the url
     */
    public function __construct()
    {
        $this->Session = Session::getInstance();
        $this->url = str_replace(WEBSITE, '', $_SERVER['REQUEST_URI']);
    }

    /**
     * Include the correct controller and action depending of the action / controller
     */
    public function dispatch()
    {
        $controller = $this->loadController($this->controller);
        if (Auth::isAuthorized()) {

            if (!method_exists($controller, $this->action)) {
                if ($this->controller != 'pages') {
                    $controller->redirect('/pages/404');
                    die();
                }
            } else {
                call_user_func_array([$controller, $this->action], $this->args);
            }

            $controller->render($this->action, $this->controller);
        } else {
            $controller->redirect('/admin');
        }
    }

    /**
     * Include the controller retrieved by the controller
     * @param $controllerToUse String Name of the controller
     * @return Instance of the controller
     */
    private function loadController($controllerToUse)
    {
        $name = ucfirst($controllerToUse) . 'Controller';
        require_once('controllers/' . $name . '.php');
        $controller = new $name($this->Session);
        return $controller;
    }

    /**
     * Perform a request from view / controller on a controller / action
     * @param $controller Controller to include
     * @param $action Action to perform
     * @param array $args Arguments for the action
     * @return mixed Result of the method
     */
    public static function requestAction($controller, $action, $args = [])
    {
        $controller = ucfirst($controller) . 'Controller';
        require_once('controllers/' . $controller . '.php');
        $actionController = new $controller();
        return call_user_func_array([$actionController, $action], $args);
    }

    /**
     * Return the current controller
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Return the current action
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    // Ajout pour nouveau routeur
} 