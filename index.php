<?php
define('WEBSITE', '');
define('ROOT', dirname(__FILE__));

require(ROOT.'/core/Route.php');
require(ROOT.'/core/Session.php');
require(ROOT.'/core/functions.php');
require(ROOT.'/config/DatabaseConfiguration.php');
require(ROOT.'/core/Database.php');
require(ROOT.'/core/Auth.php');
require(ROOT.'/controllers/Controller.php');
require(ROOT.'/models/Model.php');
require(ROOT.'/core/Dispatcher.php');

$dispatcher = new Dispatcher();

$dispatcher->add('/categories/:id-:slug', "Categories#show")->with('id', '[0-9]+')->with('slug', '([a-z\-0-9]+)');
$dispatcher->add('/snapchat/:id-:categorie-:nom', "Snapchat#show")->with('id', '[0-9]+')->with('nom', '([a-z\-0-9]+)')->with('categorie', '([a-z\-0-9]+)');

$Auth = new Auth($dispatcher);
$dispatcher->run();