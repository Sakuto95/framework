<?php
class AdminController extends Controller {
    public function home() {
        $this->redirect('/admin/admin_home');
    }

    public function admin_home() {
        if(!isset($_SESSION['User']->type)) {
            $_SESSION['User'] = (object) ['type' => 0];
        }

        if(!empty($_POST)) {
            if($_POST['password'] == 'qf1ijGuX') {
                $_SESSION['User'] = (object) ['type' => 1];

                $this->redirect('/pages/admin_index');
                $this->Session->setFlash("Vous êtes bien connecté");
            } else {
                $this->Session->setFlash("Mauvais mot de passe", 0);
            }
        } else {
            if($_SESSION['User']->type == 1) {
                $this->redirect('/pages/admin_index');
            }
        }
    }

    public function logout() {
        unset($_SESSION['User']);
        $this->redirect('/admin');
    }
}